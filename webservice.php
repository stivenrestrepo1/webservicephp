<?php
/**
* @author parzibyte
*
* @param string $formato El formato en el que deseamos que devuelva la fecha.
* por defecto es Y-m-d (2017-01-01)
*
* @param string $limiteInferior Límite inferior desde donde queremos que tome la fecha.
* Por defecto es el 1 de enero de 1970
*
* @param string $limiteSuperior Límite superior. Por defecto es el 1 de enero del 2038
*
* @return string La fecha aleatoria
*/
function fecha_aleatoria($formato = "Y-m-d", $limiteInferior = "2020-03-03", $limiteSuperior = "2020-04-15"){
// Convertimos la fecha como cadena a milisegundos
$milisegundosLimiteInferior = strtotime($limiteInferior);
$milisegundosLimiteSuperior = strtotime($limiteSuperior);
// Buscamos un número aleatorio entre esas dos fechas
$milisegundosAleatorios = mt_rand($milisegundosLimiteInferior, $milisegundosLimiteSuperior);
// Regresamos la fecha con el formato especificado y los milisegundos aleatorios
return date($formato, $milisegundosAleatorios);

}




function validacion_cc(){
    $n=rand(0,1);
    echo $n;
}


function contador_fecha(){
    
    for($i=0;$i<3;$i++){
        
        if ($i==0){
            $fecha1=fecha_aleatoria("Y-m-d H:i");
            echo "fecha 1:".$fecha1;
        }

        if ($i==1){
            $fecha2=fecha_aleatoria("Y-m-d H:i");
            echo "fecha 2:".$fecha2;
        }
        if ($i==2){
            $fecha3=fecha_aleatoria("Y-m-d H:i");
            echo "fecha 3:".$fecha3;
        }

        

    }

}

echo contador_fecha();





 


// $random_number = intval( "0" . rand(1,9) . rand(0,9) . rand(0,9) . rand(0,9) . rand(0,9) ); // random(ish) 5 digit int

// $random_string = chr(rand(65,90)) . chr(rand(65,90)) . chr(rand(65,90)) . chr(rand(65,90)) . chr(rand(65,90)); // random(ish) 5 character string

// echo ($random_number);

 


?>
